Example Bitbucket
=================

Hello World with Django

Getting Started
---------------

To start developing on this project simply bring up the Docker setup:

.. code-block:: console

    docker-compose up

Migrations will run automatically at startup (via the container entrypoint).
If they fail the very first time simply restart the application.

Open your web browser at http://localhost:8000 to see the application
you're developing.  Log output will be displayed in the terminal, as usual.

For running tests, linting, security checks, etc. see instructions in the
`tests/ <tests/README.rst>`_ folder.

Initial Setup
^^^^^^^^^^^^^

#. Create a *production*, *integration* and *development* project at the
   `VSHN Control Panel <https://control.vshn.net/openshift/projects/appuio%20public>`_.
   For quota sizing consider roughly the sum of ``limits`` of all
   resources (must be strictly greater than the sum of ``requests``):

   .. code-block:: console

        grep -A2 limits deployment/*/*/*yaml
        grep -A2 requests deployment/*/*/*yaml

#. With the commands below, create a service account from your terminal
   (logging in to your cluster first), grant permissions to push images
   and apply configurations, and get the service account's token value:
   (`APPUiO docs <https://docs.appuio.ch/en/latest/services/webserver/50_pushing_to_appuio.html>`_)

   .. code-block:: console

        oc -n example-bitbucket-production create sa bitbucket-pipelines
        oc -n example-bitbucket-production policy add-role-to-user admin -z bitbucket-pipelines
        oc -n example-bitbucket-production sa get-token bitbucket-pipelines

#. Note down service account token and your cluster's URL, and

   - at `Settings > Pipelines > Settings
     <https://bitbucket.org/appuio/example-django/admin/addon/admin/pipelines/settings>`_,
     check "Enable Pipelines",
   - at `Settings > Pipelines > Repository variables
     <https://bitbucket.org/appuio/example-django/admin/addon/admin/pipelines/repository-variables>`_
     configure the following environment variables, which allow the pipeline
     to integrate with your container platform:

     - ``KUBE_TOKEN``
     - ``KUBE_URL``

#. Rename the default deployment environments at

   - `Settings > Deployments
     <https://bitbucket.org/appuio/example-django/admin/addon/admin/pipelines/deployment-settings>`_

   as follows:

   - *Test* ➜ *Development*
   - *Staging* ➜ *Integration*

#. Grant the service account permissions on the *development* and *integration*
   projects:

   .. code-block:: console

        oc -n example-bitbucket-integration policy add-role-to-user \
          admin system:serviceaccount:example-bitbucket-production:bitbucket-pipelines
        oc -n example-bitbucket-development policy add-role-to-user \
          admin system:serviceaccount:example-bitbucket-production:bitbucket-pipelines

Integrate External Tools
^^^^^^^^^^^^^^^^^^^^^^^^

:Sentry:
  - Add environment variable ``SENTRY_DSN`` in
    `Settings > CI/CD > Variables <https://gitlab.com/appuio/example-django/-/settings/ci_cd>`_
  - Delete secrets in your namespace and run a deployment (to recreate them)
  - Configure `Error Tracking <https://gitlab.com/appuio/example-django/-/error_tracking>`_
    in `Settings > Operations > Error Tracking <https://gitlab.com/appuio/example-django/-/settings/operations>`_

Working with Docker
^^^^^^^^^^^^^^^^^^^

Create/destroy development environment:

.. code-block:: console

    docker-compose up       # --build to build containers; -d to daemonize
    docker-compose down     # docker-compose kill && docker-compose rm -af

Start/stop development environment:

.. code-block:: console

    docker-compose start    # resume after 'stop'
    docker-compose stop     # stop containers, but keep them intact

Other useful commands:

.. code-block:: console

    docker-compose ps       # list running containers
    docker-compose logs -f  # view (and follow) container logs

See the `docker-compose CLI reference`_ for other commands.

.. _docker-compose CLI reference: https://docs.docker.com/compose/reference/overview/

CI/CD Process
^^^^^^^^^^^^^

We have 3 environments corresponding to 3 namespaces on our container
platform: *development*, *integration*, *production*

- Any merge request triggers a deployment of a review app on *development*.
  When a merge request is merged or closed the review app will automatically
  be removed.
- Any change on the main branch, e.g. when a merge request is merged into
  ``master``, triggers a deployment on *integration*.
- To trigger a deployment on *production* push a Git tag, e.g.

  .. code-block:: console

    git checkout master
    git tag 1.0.0
    git push --tags

Credits
^^^^^^^

Made with ♥ by `Painless Continuous Delivery`_ Cookiecutter. This project was
generated via:

.. code-block:: console

    cookiecutter gh:painless-software/painless-continuous-delivery \
        project_name="Example Bitbucket" \
        project_description="Hello World with Django" \
        vcs_platform="Bitbucket.org" \
        vcs_account="appuio" \
        vcs_project="example-django" \
        ci_service="bitbucket-pipelines.yml" \
        cloud_platform="APPUiO" \
        cloud_account="demo4501@appuio.ch" \
        cloud_project="example-bitbucket" \
        environment_strategy="dedicated" \
        docker_registry="registry.appuio.ch" \
        framework="Django" \
        database="Postgres" \
        cronjobs="complex" \
        checks="flake8,pylint,bandit,safety,kubernetes" \
        tests="py37,behave" \
        monitoring="Sentry" \
        license="GPL-3" \
        --no-input

.. _Painless Continuous Delivery: https://github.com/painless-software/painless-continuous-delivery/
